﻿using ColorRun.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Xamarin.Forms;

namespace ColorRun.Pages
{
    public class GamePage : ContentPage
    {
        private ColorGame _game;

        public GamePage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            _game = new ColorGame(this);
            _game.Play();
        }
    }
}
