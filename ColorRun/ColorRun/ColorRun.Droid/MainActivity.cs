﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Util;

namespace ColorRun.Droid
{
    [Activity(Label = "ColorRun", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadScreenSize();
            LoadApplication(new App());
        }

        private void LoadScreenSize()
        {
            var point = new DisplayMetrics();
            this.WindowManager.DefaultDisplay.GetMetrics(point);
            AppInfo.Instance.Width = ConvertPixelsToDp(point.WidthPixels);
            AppInfo.Instance.Height = ConvertPixelsToDp(point.HeightPixels);
        }
        private int ConvertPixelsToDp(float pixelValue)
        {
            var dp = (int)((pixelValue) / Resources.DisplayMetrics.Density);
            return dp;
        }
    }
}

