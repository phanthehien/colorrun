using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ColorRun.Droid.Extend;
using Xamarin.Forms;
using ColorRun.Controls;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ColorBox), typeof(ColorBoxRenderer))]
namespace ColorRun.Droid.Extend
{
    public class ColorBoxRenderer : BoxRenderer
    {
        public ColorBoxRenderer() : base()
        {
            
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            this.SetBackgroundResource(Resource.Drawable.tags_rounded_corners);

            var drawable = (GradientDrawable) this.Background;
            drawable.SetColor(Element.Color.ToAndroid());
        }
    }
}