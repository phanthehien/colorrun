﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColorRun
{
    public class AppInfo
    {
        private static AppInfo _appInfo;

        public AppInfo()
        { 
        }
        public double Width { get; set; }

        public double Height { get; set; }

        public static AppInfo Instance
        {
            get
            { 
                if(_appInfo == null)
                {
                    _appInfo = new AppInfo();    
                }

                return _appInfo;
            }
        }
    }
    
}
