﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ColorRun.Controls
{
    public class ColorGrid : Grid
    {
        private readonly int PADDING = 2;
        private readonly Color _primaryColor, _secondaryColor;
        private readonly int _numberOfCell;
        private readonly double _boxWidth = -1;
        private readonly int _chosenPosition;

        public Color PrimaryColor
        {
            get
            {
                return _primaryColor;
            }
        }

        public Color SecondaryColor
        {
            get
            {
                return _secondaryColor;
            }
        }

        public int NumberOfCell
        {
            get
            {
                return _numberOfCell;
            }
        }

        public int ChosenPosition
        {
            get
            {
                return _chosenPosition;
            }
        }

        public double BoxWidth
        {
            get
            {
                return _boxWidth;
            }
        }

        public Action<ColorBox> BoxClick { get; set; }

        public ColorGrid(int numberOfCell, Color primaryColor, Color secondaryColor)
            : base()
        {
            _numberOfCell = numberOfCell;
            _primaryColor = primaryColor;
            _secondaryColor = secondaryColor;

            HorizontalOptions = LayoutOptions.Center;
            VerticalOptions = LayoutOptions.Center;
            Padding = PADDING * 3;
            BackgroundColor = Color.White;
            ColumnSpacing = PADDING;
            RowSpacing = PADDING;

            _boxWidth = CalculateBoxWidth(numberOfCell);

            //var columnDefinitions = new ColumnDefinitionCollection();
            //var rowDefinitions = new RowDefinitionCollection();
            //for (int i = 0; i < _numberOfCell; i++)
            //{
            //    columnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            //    rowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            //}
            //this.ColumnDefinitions = columnDefinitions;
            //this.RowDefinitions = rowDefinitions;

            _chosenPosition = RandomPosition();

            for (int i = 0; i < numberOfCell; i++)
            {
                for (int j = 0; j < numberOfCell; j++)
                {
                    var boxView = new ColorBox(this, i, j, _boxWidth);
                    boxView.Color = boxView.Position == _chosenPosition ? secondaryColor : primaryColor;
                    boxView.BoxClick = ColorBox_Click;
                    this.Children.Add(boxView, i, j);
                }
            }
        }

        private double CalculateBoxWidth(int numberOfCell)
        {
            var width = (AppInfo.Instance.Width - (numberOfCell + 1) * PADDING) / numberOfCell;
            return width;
        }

        private void ColorBox_Click(ColorBox colorBox)
        {
            if (BoxClick != null)
            {
                BoxClick(colorBox);
            }
        }

        private int RandomPosition()
        {
            var random = new Random();
            return random.Next(0, _numberOfCell * _numberOfCell - 1);

        }
    }
}
