﻿using System.Runtime.InteropServices.WindowsRuntime;
using ColorRun.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ColorRun
{
    public class ColorGame
    {
        private const int MAX_TIME = 60;
        private const int INIT_DIMENSION = 1;
        private const int MAX_DIMENSION = 10;

        private readonly object _locker = new object();


        private ColorGrid _colorGrid;
        private int _currentDimension = INIT_DIMENSION;
        private readonly ContentPage _rootPage;
        private readonly Random _random = new Random();
        private readonly StackLayout _gameLayout;
        private readonly ContentView _contentView;
        private readonly Label _lblHeading;
        private readonly Label _lblScore;
        private readonly Label _lblTimer;
        private readonly Button _btnPlay;
        private readonly Button _btnHome;
        private int _score = 0;
        private int _time = 0;
        private bool _isContinue = true;
        private double _currentOpacity = 0.75f;
        private bool _isExit = false;

        public ColorGrid Grid
        {
            get
            {
                return _colorGrid;
            }
        }

        public ColorGame(ContentPage rootPage)
        {
            _rootPage = rootPage;

            _lblHeading = new Label
            {
                Text = "Color Run", //\n(Developed by phanthehien@gmail.com)",
                TextColor = Color.Red,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                LineBreakMode = LineBreakMode.WordWrap,
                XAlign = TextAlignment.Center,
                YAlign = TextAlignment.Center
            };

            _lblScore = new Label
            {
                Text = string.Format("Your Scores: {0}", _score),
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.Center
            };

            _lblTimer = new Label
            {
                Text = string.Format("Time: {0}", MAX_TIME),
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.Center
            };

            _btnPlay = new Button()
            {
                Text = "Play",
                TextColor = Color.Red,
                BorderColor = Color.Blue,
                BackgroundColor = Color.Silver,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center
            };

            _btnHome = new Button()
            {
                Text = "Home",
                TextColor = Color.Red,
                BorderColor = Color.Blue,
                BackgroundColor = Color.Silver,
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center
            };
            _btnHome.Clicked += _btnHome_Clicked;

            _btnPlay.Clicked += _btnPlay_Clicked;
            _contentView = new ContentView();

            _gameLayout = new StackLayout()
            {
                Children = 
                {
                    _lblHeading
                    , _lblScore
                    , _lblTimer
                    , _contentView,
                    new StackLayout()
                    {
                        Children =
                        {
                            _btnHome,
                            _btnPlay
                        },
                        Orientation = StackOrientation.Horizontal
                    }
                    
                },
            };

            _rootPage.Content = _gameLayout;
        }

        public void Play()
        {
            _btnPlay.Text = "Reset";
            ResetGame();
            UpdateGrid();
        }

        private void _btnHome_Clicked(object sender, EventArgs e)
        {
            lock (_locker)
            {
                _isExit = true;
                _rootPage.Navigation.PopAsync(false);
            }
        }

        private void _btnPlay_Clicked(object sender, EventArgs e)
        {
            if (_btnPlay.Text == "Play")
            {
                Play();
            }
            else
            {
                _isContinue = false;
            }
        }

        private bool Timer_Handle()
        {
            if (_isExit)
            {
                return false;
            }

            lock (_locker)
            {
                if (_isContinue == false)
                {
                    _lblTimer.Text = "Time: 0";
                    Play();
                    return false;
                }

                _lblTimer.Text = string.Format("Time: {0}", --_time);

                if (_time <= 0 || _isContinue == false)
                {
                    _isContinue = false;
                    for (int i = 0; i < _colorGrid.Children.Count; i++)
                    {
                        _colorGrid.Children[i].GestureRecognizers.Clear();
                    }

                    _colorGrid.Animate("UpdateColor", new Animation(d =>
                    {
                        for (int i = 0; i < _colorGrid.Children.Count; i++)
                        {
                            _colorGrid.Children[i].Opacity = d;
                        }

                    }, _colorGrid.Opacity, 0, Easing.Linear, () =>
                    {
                        //_colorGrid.Children.Clear();

                    }));

                    _colorGrid.IsEnabled = false;
                    _contentView.Content = new Label()
                    {
                        Text = string.Format("Your Score: {0}", _score),
                        HeightRequest = 200,
                        TextColor = Color.Red,
                        BackgroundColor = Color.Teal,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        XAlign = TextAlignment.Center,
                        YAlign = TextAlignment.Center
                    };

                    _btnPlay.Text = "Play";
                }

                return _isContinue;
            }
        }

        private void ResetGame()
        {
            lock (_locker)
            {
                _time = MAX_TIME;
                _score = 0;
                _currentDimension = INIT_DIMENSION;
                _isContinue = true;
                Device.StartTimer(TimeSpan.FromSeconds(1), Timer_Handle);
            }
        }


        private void Box_Clicked(ColorBox colorBox)
        {
            if (colorBox.Position == _colorGrid.ChosenPosition)
            {
                _score++;
                UpdateGrid();
            }
        }

        private void UpdateGrid()
        {
            var color = CreateRandomColor();

            if (_currentDimension >= MAX_DIMENSION)
            {
                _currentDimension = _random.Next(_currentDimension - 1, _currentDimension);
            }
            else
            {
                _currentOpacity += (_currentOpacity / 5) * 0.1;
            }

            _colorGrid = null;
            _colorGrid = new ColorGrid(++_currentDimension, color, color.MultiplyAlpha(0.85f));
            _colorGrid.BoxClick += Box_Clicked;
            _contentView.Content = _colorGrid;

            _lblScore.Text = string.Format("Score: {0}", _score);
        }

        private Color CreateRandomColor()
        {
            int r = _random.Next(0, 255);
            int g = _random.Next(0, 255);
            int b = _random.Next(0, 255);

            return Color.FromRgba(r, g, b, 255);

        }
    }
}

