﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ColorRun.Pages
{
    public class AboutPage : ContentPage
    {
        public AboutPage()
            : base()
        {
            NavigationPage.SetHasNavigationBar(this, false);

            var btnBack = new Button()
                    {
                        WidthRequest = 200,
                        Text = "Back",
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                    };
            btnBack.Clicked += AboutPage_Clicked;
            this.Content = new StackLayout()
            {
                Children = 
                {
                    new Label()
                    {
                        HorizontalOptions  = LayoutOptions.CenterAndExpand,
                        VerticalOptions  = LayoutOptions.CenterAndExpand,
                        Text = "Developer: phanthehien@gmail.com"
                    },
                   btnBack
                },
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };
        }

        private void AboutPage_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PopAsync(true);
        }
    }
}
