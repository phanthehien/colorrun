﻿using System;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ColorRun.Pages
{
    public class HomePage : ContentPage
    {
        private Button _btnStart;
        private Button _btnScore;
        private Button _btnAbout;

        public HomePage()
            : base()
        {
            NavigationPage.SetHasNavigationBar(this, false);

            var heading = new Label
            {
                Text = "Color Run",
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.Fill,
                XAlign = TextAlignment.Center,
                BackgroundColor =  Color.Blue
            };

            var relativeLayout = new RelativeLayout()
            {
                BackgroundColor = Color.Purple,
                HorizontalOptions = LayoutOptions.Fill,
                VerticalOptions = LayoutOptions.Fill

            };

            _btnStart = new Button()
            {
                Text = "Start",
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 200,
            };

            _btnStart.Clicked += _btnStart_Clicked;
            _btnScore = new Button()
            {
                Text = "Scores",
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 200,
            };

            _btnAbout = new Button()
            {
                Text = "About",
                HorizontalOptions = LayoutOptions.Center,
                WidthRequest = 200,
            };
            _btnAbout.Clicked += _btnAbout_Clicked;
            var stackButtons = new StackLayout()
            {
                Children =
                {
                    _btnStart, _btnScore, _btnAbout
                },
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            relativeLayout.Children.Add(heading, widthConstraint: Constraint.RelativeToParent((parent) =>
            {
                return parent.Width;
            }));

            relativeLayout.Children.Add(stackButtons, Constraint.RelativeToParent((parent) =>
            {
                return 0;
            }), Constraint.RelativeToView(heading, (layout, view) =>
            {
                return layout.Height / 4;
            }), Constraint.RelativeToParent((parent) =>
            {
                return parent.Width;
            }), Constraint.RelativeToView(heading, (layout, view) =>
            {
                return layout.Height - view.Height;
            }));


            this.Content = relativeLayout;
        }

        private void _btnAbout_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AboutPage(), true);
        }

        private void _btnStart_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new GamePage(), false);
        }
    }
}
